signal string s1;

signal string s2 = s1;

signal string sigA;
signal string sigB;

node xyz (
	in string in,
	out string b
);

node abc <string src = "pkg:name", number x = 12> (
	in a,
	out b
	);
	
create abc <x = 15> abcInstance ();

create xyz xyzInstance (
	.in(sigA),
	.b(sigB)
	);
	
create xyz xyzInstance2 ();
	
connect (
	xyzInstance2.in(sigA),
	s1(xyzInstance2.b)
	);