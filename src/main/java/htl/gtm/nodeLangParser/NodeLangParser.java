/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package htl.gtm.nodeLangParser;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import htl.gtm.nodeLangParser.exception.NodeLangException;
import htl.gtm.nodeLangParser.exception.NodeLangFormatException;
import htl.gtm.nodeLangParser.exception.NodeNotDefinedException;
import htl.gtm.nodeLangParser.exception.SignalDriveException;
import htl.gtm.nodeLangParser.exception.SignalNotDeclaredException;
import htl.gtm.nodeLangParser.exception.TypeMismatchException;
import htl.gtm.nodeLangParser.exception.VectorWidthMismatchException;
import htl.gtm.nodeLangParser.signal.LuaVariable;
import htl.gtm.nodeLangParser.signal.Port;
import htl.gtm.nodeLangParser.signal.Signal;
import htl.gtm.nodeLangParser.signal.Signal.Type;
import htl.gtm.nodeLangParser.signal.Wire;
import htl.gtm.nodeLangParser.util.Tuple;

/**
 * Used for parsing NodeLang files to representational {@link Map}s of {@link Map}<{@link String}, {@link Signal}>, {@link Map}<{@link String}, {@link Node}> and {@link Map}<{@link String}, {@link Instance}>
 * 
 * @author Clemens Lechner
 *
 */
public class NodeLangParser {

	private Pattern sectionNodePattern = Pattern
			.compile("\\s*node\\s+(?<name>[a-zA-Z]+\\w*)\\s*(<\\s*(?<fields>.*)\\s*>\\s*)?\\((?<portlist>.*)\\).*");

	private Pattern sectionInstancePattern = Pattern.compile(
			"\\s*create\\s+(?<node>[a-zA-Z]+\\w*)(\\s*<\\s*(?<fields>.*)\\s*>)?\\s+(?<name>[a-zA-Z]+\\w*)\\s*\\((?<ports>.*)\\).*");
	private Pattern sectionConnectPattern = Pattern.compile("\\s*connect\\s*\\(\\s*(?<portlist>.*)\\s*\\).*");

	private Pattern includePattern = Pattern.compile("\\s*include\\s+\"(.*)\".*");

	private Pattern signalDefinitionPattern = Pattern.compile(
			"\\s*signal(\\s+(?<type>nil|boolean|number|string|function|userdata|thread|table))?(\\s*\\[(?<array>[\\d:])\\])?\\s+(?<name>[a-zA-Z]+[\\w.]*)(\\s*=\\s*(?<assignee>[a-zA-Z]+[\\w.]*)(\\s*\\[(?<assarray>\\d:)\\])?)?.*");

	private Pattern inoutPattern = Pattern.compile(
			"\\s*(?<io>in|out)\\s+((?<type>nil|boolean|number|string|function|userdata|thread|table)\\s+)?(?<name>[a-zA-Z]+\\w*)\\s*");
	private Pattern fieldsPattern = Pattern
			.compile("\\s*((?<type>[a-zA-Z]+\\w*)\\s+)?(?<name>[a-zA-Z]+\\w*)\\s*=\\s*(?<value>.*)\\s*");

	private Pattern instanceConnectPattern = Pattern
			.compile("\\s*.([a-zA-Z]+\\w*)\\s*\\(\\s*([a-zA-Z]+[\\w\\.]*)\\s*\\)\\s*");
	private Pattern sectionConnectSubPattern = Pattern
			.compile("\\s*([a-zA-Z]+[\\w\\.]*)\\s*\\(\\s*([a-zA-Z]+[\\w\\.]*)\\s*\\)\\s*");

	private Path targetPath;
	private Path workingDirectory = null;

	private Map<String, Signal> signalMap = new HashMap<>();
	private Map<String, Node> nodes = new HashMap<>();
	private Map<String, Instance> instances = new HashMap<>();

	private List<String> includedFiles = new ArrayList<>();

	public NodeLangParser() {

	}

	/**
	 * Only used for passing arguments at include statemant
	 * 
	 * @param includedFiles
	 * @param workingDirectory
	 * @param targetPath
	 * @param signalMap
	 * @param nodes
	 * @param instances
	 */
	private NodeLangParser(List<String> includedFiles, Path workingDirectory, Path targetPath,
			Map<String, Signal> signalMap, Map<String, Node> nodes, Map<String, Instance> instances) {
		this.includedFiles = includedFiles;
		this.workingDirectory = workingDirectory;
		this.targetPath = targetPath;
		this.signalMap = signalMap;
		this.nodes = nodes;
		this.instances = instances;
	}

	/**
	 * @return the path of the target file to parse
	 */
	public Path getTargetPath() {
		return targetPath;
	}

	/**
	 * Used to set the path to the target file for parsing
	 * @param targetPath
	 */
	public void setTargetPath(Path targetPath) {
		this.targetPath = targetPath;
	}

	/**
	 * A map of global signals
	 * @return A map of global signals
	 */
	public Map<String, Signal> getSignalMap() {
		return Collections.unmodifiableMap(signalMap);
	}

	/**
	 * A map of all globally defined nodes
	 * @return A map of all globally defined nodes
	 */
	public Map<String, Node> getNodes() {
		return Collections.unmodifiableMap(nodes);
	}

	/**
	 * A map of all globally instantiated nodes
	 * @return A map of all globally instantiated nodes
	 */
	public Map<String, Instance> getInstances() {
		return Collections.unmodifiableMap(instances);
	}

	/**
	 * Parses NodeLang code to corresponding {@link Map}s of {@link Map}<{@link String}, {@link Signal}>, {@link Map}<{@link String}, {@link Node}> and {@link Map}<{@link String}, {@link Instance}>
	 * 
	 * @throws IOException
	 * @throws NodeLangException
	 */
	public void parse() throws IOException, NodeLangException {
		if (workingDirectory == null) {
			workingDirectory = targetPath.getParent();
		}
		InputStream fis = Files.newInputStream(targetPath);

		boolean isQuotationMark = false;
		boolean lastIsBackslash = false;

		StringBuilder sb = new StringBuilder();

		int firstCourly = -1;
		int lastCourly = -1;

		char c;
		int count = 0;
		int courlyBracesCount = 0;
		boolean lastIsSlash = false;
		boolean lastIsStar = false;
		boolean isComment = false;
		boolean isMultiLineComment = false;
		while (fis.available() > 0) {
			c = (char) fis.read();
			if (c == '"' && !lastIsBackslash)
				isQuotationMark = !isQuotationMark;

			if (c == '\\')
				lastIsBackslash = true;
			else
				lastIsBackslash = false;

			if (!isQuotationMark) {
				if (c == '/') {
					if (lastIsSlash && !isComment) {
						isComment = true;
						isMultiLineComment = false;
						sb.deleteCharAt(sb.length()-1);
					} else if (lastIsStar) {
						isComment = false;
						isMultiLineComment = false;
						continue;
					}
					lastIsSlash = true;
				} else if (c == '*') {
					if (lastIsSlash) {
						isComment = true;
						isMultiLineComment = true;
						sb.deleteCharAt(sb.length() - 1);
					}
					lastIsStar = true;
					lastIsSlash = false;
				} else if (c == '\n') {
					if (isComment && !isMultiLineComment)
						isComment = false;
					lastIsSlash = false;
					lastIsStar = false;
				} else if (!isComment) {
					lastIsSlash = false;
					lastIsStar = false;
					if (c == '{') {
						if (courlyBracesCount == 0)
							firstCourly = count;
						courlyBracesCount++;
					} else if (c == '}') {
						courlyBracesCount--;
						if (courlyBracesCount == 0)
							lastCourly = count;
					} else if (c == ';' && courlyBracesCount == 0) {
						String section = sb.toString();
	
						createSection(section, firstCourly, lastCourly, signalMap);
	
						count = 0;
						lastCourly = -1;
						firstCourly = -1;
						courlyBracesCount = 0;
						sb = new StringBuilder();
						continue;
					}
				}
			}
			if (!isComment) {
				if ((c == '\n' || c == '\r') && firstCourly == -1)
					sb.append(' ');
				else
					sb.append(c);
				count++;
			}
		}
		fis.close();

	}

	/**
	 * Parses the beforehand seperated sections to its corresponding counterparts
	 * 
	 * @param section
	 * @param firstCourly position of the first Courly Brace (Used for behaviour in nodes)
	 * @param lastCourly position of the last Courly Brace (Used for behaviour in nodes)
	 * @param signalMap
	 * @throws IOException
	 * @throws NodeLangException
	 */
	private void createSection(String section, int firstCourly, int lastCourly, Map<String, Signal> signalMap)
			throws IOException, NodeLangException {
		String matcherSection;
		if (firstCourly != -1)
			matcherSection = section.substring(0, firstCourly);
		else
			matcherSection = section;

		Matcher m = sectionNodePattern.matcher(matcherSection);
		if (m.matches()) {
			Node node = new Node();

			node.setId(m.group("name"));

			String[] inouts = m.group("portlist").split(",");

			for (String s : inouts) {
				Matcher iom = inoutPattern.matcher(s);
				if (!iom.matches())
					throw new NodeLangFormatException("", section);
				String io = iom.group("io");
				String type = iom.group("type");
				Signal.Type t = Signal.Type.USERDATA;
				if (type != null) {
					t = Type.parseType(type);
				}
				if (io.equals("in"))
					node.getInputs().add(new Tuple<String, Signal.Type>(iom.group("name"), t));
				else
					node.getOutputs().add(new Tuple<String, Signal.Type>(iom.group("name"), t));
			}

			if (m.group("fields") != null) {
				// TODO remove split and add colon in string ability
				String[] fields = m.group("fields").split(",");
				for (String s : fields) {
					Matcher m1 = fieldsPattern.matcher(s);
					if (m1.matches()) {
						LuaVariable var = new LuaVariable();
						if (m1.group("type") != null) {
							var.setType(Type.parseType(m1.group("type")));
						}

						if (var.getType() == Type.STRING) {
							String vs = m1.group("value");
							if (vs.charAt(0) == '\"' && vs.charAt(vs.length() - 1) == '\"') {
								var.setVar(vs.substring(1, vs.length() - 1));
							}
						} else {
							var.setVar(m1.group("value"));
						}
						node.getFields().put(m1.group("name"), var);
					} else {
						throw new NodeLangFormatException("Syntax error: \"" + s + "\"", matcherSection);
					}
				}
			}

			if (firstCourly >= 0) {
				node.setBehaviour(section.substring(firstCourly + 1, lastCourly));
			}
			nodes.put(node.getId(), node);

			return;
		}

		m = sectionInstancePattern.matcher(matcherSection);
		if (m.matches()) {
			Instance instance = new Instance();

			Node parent = nodes.get(m.group("node"));
			if (parent == null)
				throw new NodeNotDefinedException(
						"Node named \"" + m.group("node") + "\" not defined!", section);

			instance.createFromNode(parent);

			instance.setName(m.group("name"));

			Iterator<Entry<String, Port>> it = instance.getPortlist().entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry<String, Port> entry = it.next();
				signalMap.put(entry.getValue().getName(), entry.getValue());
			}

			if (m.group("fields") != null) {
				String[] fields = m.group("fields").split(",");
				for (String s : fields) {
					Matcher m1 = fieldsPattern.matcher(s);
					if (m1.matches()) {
						LuaVariable var = new LuaVariable();
						if (instance.getFields().get(m1.group("name")) != null) {
							Type originalType = instance.getFields().get(m1.group("name")).getType();
							instance.getFields().remove(m1.group("name"));

							if (m1.group("type") != null) {
								var.setType(Signal.Type.parseType(m1.group("type")));
							} else {
								var.setType(originalType);
							}

							if (var.getType() != originalType)
								throw new TypeMismatchException(var.getType(), originalType, section);

							var.setVar(m1.group("value"));

							instance.getFields().put(m1.group("name"), var);
						} else {
							throw new NodeLangFormatException("Syntax error: \"" + s + "\"", matcherSection);
						}
					} else {
						throw new NodeLangFormatException("Syntax error: " + s, section);
					}
				}
			}

			instances.put(instance.getName(), instance);

			if (m.group("ports").equals(""))
				return;

			String[] split = m.group("ports").split(",");
			for (String s : split) {
				Matcher m1 = instanceConnectPattern.matcher(s);
				if (!m1.matches())
					throw new NodeLangFormatException("Malformed syntax!", section);

				connectFromNames(instance.getName() + "." + m1.group(1), m1.group(2), section);
			}

			return;
		}

		m = sectionConnectPattern.matcher(matcherSection);
		if (m.matches()) {
			String[] split = m.group("portlist").split(",");
			for (String s : split) {
				Matcher m1 = sectionConnectSubPattern.matcher(s);
				if (!m1.matches())
					throw new NodeLangFormatException("Malformed syntax!", section);
				connectFromNames(m1.group(1), m1.group(2), section);
			}

			return;
		}

		m = includePattern.matcher(matcherSection);
		if (m.matches()) {
			// Get Path to include file and relativize according to working directory
			Path includePath = workingDirectory.relativize(targetPath.getParent().resolve(m.group(1)));
			if (includedFiles.contains(includePath.toString()))
				return;

			includedFiles.add(includePath.toString());
			NodeLangParser includeParser = new NodeLangParser(includedFiles, workingDirectory,
					workingDirectory.resolve(includePath), signalMap, nodes, instances);
			includeParser.parse();

			return;
		}

		m = signalDefinitionPattern.matcher(matcherSection);
		if (m.matches()) {
			boolean isAssignment = m.group("assignee") != null;
			String name = m.group("name");

			Type type = Type.USERDATA;
			if (m.group("type") != null) {
				type = Type.parseType(m.group("type"));
				if (type == null)
					throw new NodeLangFormatException("Syntax Error: " + m.group("type")
							+ " is no valid type!", section);
			}

			if (m.group("array") != null) {
				String[] bounds = m.group("array").split(":");
				if (bounds.length != 2)
					throw new NodeLangFormatException(
							"Syntax Error: \"" + m.group("array") + "\"", section);
				int firstBound = Integer.parseUnsignedInt(bounds[0]);
				int secondBound = Integer.parseInt(bounds[1]);
				if (firstBound > secondBound) {
					int i = firstBound;
					firstBound = secondBound;
					secondBound = i;
				}
				for (int i = firstBound; i < secondBound; i++) {
					Wire s = new Wire(name + "$" + i, type);
					if (isAssignment) {
						if (m.group("assarray") == null)
							throw new VectorWidthMismatchException(
									"Can't assign vector \"" + m.group("assignee") + "\" with width of 1 to vector \""
											+ name + "\" with width of " + Integer.toString(secondBound - firstBound)
											+ "!", section);
						String[] asBounds = m.group("assarray").split(":");
						if (asBounds.length != 2)
							throw new NodeLangFormatException("Syntax error: " + m.group("assarray"), section);
						int asFirstBound = Integer.parseUnsignedInt(asBounds[0]);
						int asSecondBound = Integer.parseInt(asBounds[1]);
						if (asFirstBound > asSecondBound) {
							int x = asFirstBound;
							asFirstBound = asSecondBound;
							asSecondBound = x;
						}
						if ((asSecondBound - asFirstBound) != (secondBound - firstBound))
							throw new VectorWidthMismatchException(
									"Can't assign vector \"" + m.group("assignee") + "\" with width of " + Integer.toString(asSecondBound - asFirstBound) + " to vector \""
											+ name + "\" with width of " + Integer.toString(secondBound - firstBound)
											+ "!", section);
						Signal other = signalMap.get(m.group("assarray") + "$" + (i - firstBound + asFirstBound));
						if (other == null)
							throw new SignalNotDeclaredException("Signal \"" + m.group("assignee") + "\" used but not declared!", matcherSection);
						s.connect(other, section);
					}
					signalMap.put(s.getName(), s);
				}
			} else {
				Wire s = new Wire(name, type);
				if (isAssignment) {
					if (m.group("assarray") != null)
						throw new VectorWidthMismatchException("Can't assign vector \"" + m.group("assignee") + "\" with width other than 1 to signal \"" + name + "\" with width of 1!", section);
					Signal asignee = signalMap.get(m.group("assignee"));
					if (asignee == null)
						throw new SignalNotDeclaredException("Signal \"" + m.group("assignee") + "\" used but not declared!", matcherSection);
					s.connect(asignee, section);
				}
				signalMap.put(name, s);
			}
			return;
		}

		throw new NodeLangFormatException("Malformed syntax!", matcherSection);
	}

	/**
	 * Tries to connect from signal names (keys of SignalMap)
	 * 
	 * @param s1 the name of the first signal to connect
	 * @param s2 the name of the second signal to connect
	 * @param section the section that tries to connect both signals (used for error handling)
	 * @throws SignalNotDeclaredException
	 * @throws TypeMismatchException
	 * @throws SignalDriveException
	 */
	private void connectFromNames(String s1, String s2, String section) throws SignalNotDeclaredException, TypeMismatchException, SignalDriveException {
		Signal sig1 = signalMap.get(s1);
		if (sig1 == null) {
			throw new SignalNotDeclaredException("Signal \"" + s1 + "\" used but not declared!", section);
		}

		Signal sig2 = signalMap.get(s2);
		if (sig2 == null) {
			throw new SignalNotDeclaredException("Signal \"" + s2 + "\" used but not declared!", section);
		}

		sig1.connect(sig2, section);
	}

	/**
	 * 
	 * @return the path of the working directory (folder that the root target file is in)
	 */
	public Path getWorkingDirectory() {
		return workingDirectory;
	}

	/**
	 * 
	 * @param workingDirectory the path of the working directory (folder that the root target file is in)
	 */
	public void setWorkingDirectory(Path workingDirectory) {
		this.workingDirectory = workingDirectory;
	}

	public void setTargetPath(String string) {
		setTargetPath(Paths.get(string));

	}

}
