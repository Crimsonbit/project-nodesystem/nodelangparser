/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package htl.gtm.nodeLangParser.signal;

import htl.gtm.nodeLangParser.Instance;

/**
 * Superclass for {@link Input} and {@link Output}
 * 
 * @author Clemens Lechner
 *
 */
public abstract class Port extends Signal {
	
	public enum Direction {
		IN,
		OUT
	}
	
	private Instance parent;
	private Direction dir;

	public Port(String name, Type type, Direction dir, Instance parent) {
		super(name, type);
		this.setDir(dir);
		this.parent = parent;
	}
	
	public Port(String name, Direction dir, Instance parent) {
		super(name);
		this.setDir(dir);
		this.parent = parent;
	}

	@Override
	public String getName() {
		return parent.getName() + "." + super.getName();
	}
	
	public String getLocalName() {
		return super.getName();
	}

	public Direction getDir() {
		return dir;
	}

	public void setDir(Direction dir) {
		this.dir = dir;
	}
	
	public Instance getParent() {
		return parent;
	}

}
