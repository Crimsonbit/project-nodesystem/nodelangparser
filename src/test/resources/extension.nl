signal s1;

signal s2 = a;

signal s4[8:0];

signal s3[3:0] = z[8:5];

node uvw (
	in a,
	out b
);