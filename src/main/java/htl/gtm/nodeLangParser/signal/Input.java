/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package htl.gtm.nodeLangParser.signal;

import htl.gtm.nodeLangParser.Instance;

/**
 * Wrapper for a {@link Instance}'s Input
 * 
 * @see Consumer
 * 
 * @author Clemens Lechner
 *
 */
public class Input extends Port implements Consumer {
	
	private Producer producer;

	public Input(String name, Instance parent) {
		super(name, Port.Direction.IN, parent);
	}

	public Input(String name, Type type, Instance parent) {
		super(name, type, Port.Direction.IN, parent);
	}

	@Override
	public void setProducer(Producer p) {
		this.producer = p;		
	}

	@Override
	public Producer getProducer() {
		return producer;
	}

}
