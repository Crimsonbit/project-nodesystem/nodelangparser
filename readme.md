# NodeLang
The following document contains the definition for "nodeLang" a language for describing netlists of abstractized node elements.
Version: 0.1.3
Date: 13.10.2018 (2018/10/13)
Author: Clemens Lechner
Edited for GitLab: Florian Wagner

## Example Node Definition
port section describes interface

### behaviour section describes behaviour of the node (for example mathematical expressions)
```
node xyz (
	in number a,
	out string b
)
{
	//insert node behaviour
};
```
### Example for extern Node definition
```
node xyz <src=nodepackage:nodename, string constname="const">;
```
### Example for abstract Node definition
```
node xyz (
	in a,
	out b
);
```
### Import statement for external files
```
include "path";
```
### simple Node instantiation of previously defined node <xyz>
```
create xyz name ();
```
### simple Node instantiation of previously defined node <xyz> with initial connect
```
create xyz name (
	.a(<some signal>),
	.b(<some other signal>)
	);
```
### connect statement
```
connect (
	node1.a(node2.b),
	node1.b(node2.a)
	);
```
### signal definition
```
signal s1;
```
### signal definition with initial value
```
signal s1 = node1.a;
```
### signal vector definition with variable width
```
signal[4:0] v1;
```

#Example 
```
include "extPack/someNodes.nl";

signal boolean isEqual;

create compareNumbers comp ();

create add2Numbers adder();

create constantNumber <const = 3> adderAInput (
	.out(adder.a)
	);

create constantNumber <const = 2> adderBInput (
	.out(adder.b)
	);

create addConstantValue <const = 3> add3toMe ();

create constantNumber <const = 2> add3Input (
	.out(add3toMe.input)
	);

create subtract2Numbers subtractor(
	.a(adder.c),
	.b(add3toMe.out)
	);
	
signal number subOut = subtractor.c;

create constantNumber <const = 0> comperatorA ();

connect (
	comp.a(comperatorA.out),
	comp.b(subtractor.c),
	isEqual(comp.equal)
	);
```