/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package htl.gtm.nodeLangParser.signal;

import java.util.List;

/**
 * Interface that enables a {@link Signal} to accept a {@link List} of {@link Consumer}s
 * 
 * @author Clemens Lechner
 *
 */
public interface Producer {
	
	public void setConsumers(List<Consumer> c);
	
	public List<Consumer> getConsumers();

}
