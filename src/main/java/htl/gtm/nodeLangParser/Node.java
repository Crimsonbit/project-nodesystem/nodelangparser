/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package htl.gtm.nodeLangParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import htl.gtm.nodeLangParser.signal.LuaVariable;
import htl.gtm.nodeLangParser.signal.Signal;
import htl.gtm.nodeLangParser.util.Tuple;

/**
 * Wrapper for a definition of a node
 * 
 * @author Clemens Lechner
 *
 */
public class Node {
	
	String id;
	List<Tuple<String, Signal.Type>> inputs = new ArrayList<Tuple<String, Signal.Type>>();
	List<Tuple<String, Signal.Type>> outputs = new ArrayList<Tuple<String, Signal.Type>>();
	Map<String, LuaVariable> fields = new HashMap<>();
	String behaviour;

	public Map<String, LuaVariable> getFields() {
		return fields;
	}

	public void setFields(Map<String, LuaVariable> fields) {
		this.fields = fields;
	}

	public String getBehaviour() {
		return behaviour;
	}

	public void setBehaviour(String behaviour) {
		this.behaviour = behaviour;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<Tuple<String, Signal.Type>> getInputs() {
		return inputs;
	}

	public void setInputs(List<Tuple<String, Signal.Type>> inputs) {
		this.inputs = inputs;
	}

	public List<Tuple<String, Signal.Type>> getOutputs() {
		return outputs;
	}

	public void setOutputs(List<Tuple<String, Signal.Type>> outputs) {
		this.outputs = outputs;
	}

}
