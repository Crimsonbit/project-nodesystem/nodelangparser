/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package htl.gtm.nodeLangParser;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

public class ParserLuaTest {
	
	@Test
	void test() throws URISyntaxException  {
		NodeLangParser parser = new NodeLangParser();
		parser.setTargetPath(Paths.get("src", "test", "resources", "lua.nl"));
		try {
			parser.parse();
			
			Map<String, Object> inputs = new HashMap<>();
			Map<String, Object> outputs = new HashMap<>();
			
			inputs.put("a", 2);
			inputs.put("b", 3);
			
			outputs.put("c", 1);
			
			Instance inst = parser.getInstances().get("luaInstance");
			inst.execute(inputs, outputs);
			
			assertTrue((long) outputs.get("c") == 5);
			
			System.out.println(outputs.get("c"));
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		//fail("is hoid so");
	}

}
