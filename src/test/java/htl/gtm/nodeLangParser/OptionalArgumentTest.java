/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package htl.gtm.nodeLangParser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.jupiter.api.Test;

public class OptionalArgumentTest {
	
	@Test
	public void test() {
		Pattern inoutPattern = Pattern.compile("\\s*(?<io>in|out)\\s+((?<type>nil|boolean|number|string|function|userdata|thread|table)\\s+)?(?<name>[a-zA-Z]+\\w*)\\s*");
		
		String input = "out number s";
		
		Matcher m = inoutPattern.matcher(input);
		if (m.matches()) {
			System.out.println("Matches");
			System.out.println(m.group("io"));
			System.out.println(m.group("type"));
			System.out.println();
		}
		
		input = "node n <string src = \"abc\", number xyz = 13> (asd)";
		
		Pattern patt = Pattern.compile("\\s*node\\s+(?<name>[a-zA-Z]+\\w*)\\s*(<\\s*(?<fields>.*)\\s*>\\s*)?\\((?<portlist>.+)\\).*");
		
		m = patt.matcher(input);
		
		if (m.matches()) {
			System.out.println("Second Matches");
			System.out.println(m.group("fields"));
		}
		
	}

}
