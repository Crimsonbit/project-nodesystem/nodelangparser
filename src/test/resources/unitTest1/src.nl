include "extPack/someNodes.nl";

signal boolean isEqual;

create compareNumbers comp ();

create add2Numbers adder();

create constantNumber <const = 3> adderAInput (
	.out(adder.a)
	);

create constantNumber <const = 2> adderBInput (
	.out(adder.b)
	);

create addConstantValue <const = 3> add3toMe ();

create constantNumber <const = 2> add3Input (
	.out(add3toMe.input)
	);

create subtract2Numbers subtractor(
	.a(adder.c),
	.b(add3toMe.out)
	);
	
signal number subOut = subtractor.c;

create constantNumber <const = 0> comperatorA ();

connect (
	comp.a(comperatorA.out),
	comp.b(subtractor.c),
	isEqual(comp.equal)
	);