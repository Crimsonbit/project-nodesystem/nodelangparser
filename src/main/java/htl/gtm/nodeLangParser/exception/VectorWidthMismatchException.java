/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package htl.gtm.nodeLangParser.exception;

/**
 * Thrown when trying to connect {@linkplain Signal} vectors with not matching widths
 * 
 * @author Clemens Lechner
 *
 */
public class VectorWidthMismatchException extends NodeLangException {

	private static final long serialVersionUID = -3146554145686327034L;

	public VectorWidthMismatchException() {
		super();
	}

	public VectorWidthMismatchException(String message, String section) {
		super(message, section);
	}

}
