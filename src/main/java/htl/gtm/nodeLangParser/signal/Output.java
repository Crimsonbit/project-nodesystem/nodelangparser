/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package htl.gtm.nodeLangParser.signal;

import java.util.ArrayList;
import java.util.List;

import htl.gtm.nodeLangParser.Instance;

/**
 * Wraper for {@link Instance}'s Output
 * 
 * @see Producer
 * 
 * @author Clemens Lechner
 *
 */
public class Output extends Port implements Producer {
	
	private List<Consumer> consumers = new ArrayList<>();

	public Output(String name, Instance parent) {
		super(name, Port.Direction.OUT, parent);
	}

	public Output(String name, Type type, Instance parent) {
		super(name, type, Port.Direction.OUT, parent);
	}

	@Override
	public void setConsumers(List<Consumer> c) {
		this.consumers = c;
		
	}

	@Override
	public List<Consumer> getConsumers() {
		return consumers;
	}

}
