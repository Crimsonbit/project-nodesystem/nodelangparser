/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package htl.gtm.nodeLangParser.signal;

import htl.gtm.nodeLangParser.signal.Signal.Type;

/**
 * Contains an {@link Object} and a {@link Type}
 * 
 * @author Clemens Lechner
 *
 */
public class LuaVariable {

	private Object var;
	private Type type = Type.USERDATA;

	public LuaVariable(Object var, Type type) {
		this.type = type;
		setVar(var);
	}

	public LuaVariable(String var, Type type) {
		this.type = type;
		setVar(var);
	}

	/**
	 * field type defaults to {@link Type}.USERDATA
	 * 
	 * @param var
	 */
	public LuaVariable(Object var) {
		super();
		this.var = var;
	}

	public LuaVariable() {

	}

	public Object getVar() {
		return var;
	}

	public void setVar(Object var) {
		this.var = var;
	}

	public void setVar(String var) {
		switch (this.type) {
		case BOOLEAN:
			this.var = Boolean.parseBoolean(var);
			break;
		case NUMBER:
			this.var = Double.parseDouble(var);
			break;
		case NIL:
			this.var = null;
			break;
		default:
			this.var = var;
			break;
		}
	}

	public Signal.Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return var.toString();
	}

}
