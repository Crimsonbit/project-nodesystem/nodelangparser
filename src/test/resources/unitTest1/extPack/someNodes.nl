
node constantNumber <number const = 0> (
	out number out
	)
	{
	
	output.out = fields.const;
	
	};

node add2Numbers (
	in number a,
	in number b,
	out number c
	)
	{
	
	output.c = input.a + input.b;
	
	};

node addConstantValue <number const = 0> (
	in number input,
	out number out
	)
	{
	
	output.out = input.input + fields.const;
	
	};
	
	
node subtract2Numbers (
	in number a,
	in number b,
	out number c
	)
	{
	
	output.c = input.a - input.b;
	
	};
	
node concat2Strings (
	in string s1,
	in string s2,
	out string s3
	)
	{
	
	output.s3 = input.s1 .. input.s2;
	
	};

node endString <string end = ""> (
	in string si,
	out string so
	)
	{
	
	output.so = input.si .. fields.end;
	
	};
	
node compareNumbers (
	in number a,
	in number b,
	out boolean equal,
	out boolean greater,
	out boolean smaller
	)
	{
	
	output.equal = (input.a == input.b);
	output.greater = (input.a > input.b);
	output.smaller = (input.a < input.b);
	
	};
	
node doingSomethingCrazy <src = crzypkg:crzynode> (
	in a,
	out b
);