/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package htl.gtm.nodeLangParser;

import static org.junit.jupiter.api.Assertions.*;

import java.io.FileNotFoundException;
import java.net.URISyntaxException;
import java.nio.file.Paths;

import org.junit.jupiter.api.Test;

class TestParser {

	@Test
	void test() throws URISyntaxException  {
		NodeLangParser parser = new NodeLangParser();
		parser.setTargetPath(Paths.get("src", "test", "resources", "src2.nl"));
		try {
			parser.parse();
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		//fail("is hoid so");
	}
	
//	public static void main(String args[]) {
//		new TestParser().test();
//	}

}
