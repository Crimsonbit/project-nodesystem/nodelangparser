/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package htl.gtm.nodeLangParser.lua;

import org.junit.jupiter.api.Test;

import net.sandius.rembulan.StateContext;
import net.sandius.rembulan.Table;
import net.sandius.rembulan.Userdata;
import net.sandius.rembulan.Variable;
import net.sandius.rembulan.compiler.CompilerChunkLoader;
import net.sandius.rembulan.env.RuntimeEnvironments;
import net.sandius.rembulan.exec.CallException;
import net.sandius.rembulan.exec.CallPausedException;
import net.sandius.rembulan.exec.DirectCallExecutor;
import net.sandius.rembulan.impl.DefaultTable;
import net.sandius.rembulan.impl.ImmutableTable;
import net.sandius.rembulan.impl.StateContexts;
import net.sandius.rembulan.lib.StandardLibrary;
import net.sandius.rembulan.load.ChunkLoader;
import net.sandius.rembulan.load.LoaderException;
import net.sandius.rembulan.runtime.LuaFunction;

public class LuaTest {
	
	@Test
	public void luaFunctionCall() throws LoaderException, CallException, CallPausedException, InterruptedException {
		StateContext state = StateContexts.newDefaultInstance();
		
		Table env = StandardLibrary.in(RuntimeEnvironments.system()).installInto(state);
		InputUserdata ud = new InputUserdata();
		ud.setUserValue(1);
		
		Variable v = new Variable(1);
		
		env.rawset("object",v);
		
		
		String program = "print(object)\n" + "a = 'hello'\n" + "b = {'x', 1.2, 'z'}\n" + "print(type(b[2]))\n" + "print(type(object))\n" + "local function test()print('hallo');end\n" + "test()\n";
		
		ChunkLoader loader = CompilerChunkLoader.of("example");
		LuaFunction main = loader.loadTextChunk(new Variable(env), "example", program);
		
		DirectCallExecutor.newExecutor().call(state, main);
		
		env.rawset("a", 5);
		
		System.out.println(env.rawget("a"));
		
		System.out.println(((DefaultTable) env.rawget("b")));
		
		System.out.println(((DefaultTable) env.rawget("b")).rawget(3).getClass());
		
	}
	
	private static class InputUserdata extends Userdata {
		
		private Object userData;
		private static final Table mt = new ImmutableTable.Builder().build();

		@Override
		public Object getUserValue() {
			return userData;
		}

		@Override
		public Object setUserValue(Object value) {
			Object store = userData;
			this.userData = value;
			return store;
		}

		@Override
		public Table getMetatable() {
			return mt;
		}

		@Override
		public Table setMetatable(Table mt) {
			throw new UnsupportedOperationException();
		}
		
	}

}
