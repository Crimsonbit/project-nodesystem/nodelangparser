/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package htl.gtm.nodeLangParser;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import htl.gtm.nodeLangParser.signal.Input;
import htl.gtm.nodeLangParser.signal.LuaVariable;
import htl.gtm.nodeLangParser.signal.Output;
import htl.gtm.nodeLangParser.signal.Port;
import htl.gtm.nodeLangParser.signal.Signal;
import htl.gtm.nodeLangParser.util.Tuple;
import net.sandius.rembulan.ByteString;
import net.sandius.rembulan.StateContext;
import net.sandius.rembulan.Table;
import net.sandius.rembulan.Variable;
import net.sandius.rembulan.compiler.CompilerChunkLoader;
import net.sandius.rembulan.env.RuntimeEnvironments;
import net.sandius.rembulan.exec.CallException;
import net.sandius.rembulan.exec.CallPausedException;
import net.sandius.rembulan.exec.DirectCallExecutor;
import net.sandius.rembulan.impl.StateContexts;
import net.sandius.rembulan.lib.StandardLibrary;
import net.sandius.rembulan.load.ChunkLoader;
import net.sandius.rembulan.load.LoaderException;
import net.sandius.rembulan.runtime.LuaFunction;

/**
 * <p>
 * Wrapper for an instance of a node<br>
 * </p>
 * <p>
 * {@link String} name: The name of the instance<br>
 * {@link Node} parent: the corresponding {@link Node} that the instance was created from<br>
 * {@link Map}<{@link String}, {@link Port}> portlist: map of {@link Input}s and {@link Output}s<br>
 * {@link Map}<{@link String}, {@link LuaVariable}> fields: Map of the instance's constant fields
 * </p>
 * 
 * @author Clemens Lechner
 *
 */
public class Instance {

	private String name;
	private Node parent;
	private Map<String, Port> portlist = new HashMap<>();
	private Map<String, LuaVariable> fields = new HashMap<>();
	
	public Map<String, Port> getPortlist() {
		return portlist;
	}

	public void setPortlist(Map<String, Port> portlist) {
		this.portlist = portlist;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Node getParent() {
		return parent;
	}

	public void setParent(Node parent) {
		this.parent = parent;
	}
	
	/**
	 * Used to create an instance from a {@link Node}
	 * 
	 * @param node the node to create the instance from
	 */
	public void createFromNode(Node node) {
		this.parent = node;
		for (Tuple<String, Signal.Type> t : node.getInputs()) {
			portlist.put(t.a, new Input(t.a, t.b, this));
		}
		for (Tuple<String, Signal.Type> t : node.getOutputs()) {
			portlist.put(t.a, new Output(t.a, t.b, this));
		}
		this.fields = new HashMap<>(node.getFields());
	}

	public Map<String, LuaVariable> getFields() {
		return fields;
	}

	public void setFields(Map<String, LuaVariable> fields) {
		this.fields = fields;
	}
	
	/**
	 * Executes the node's behaviour in a Lua environment
	 * 
	 * @param input map of inputs with local name as key
	 * @param outputs map of outputs with local name as key
	 * @throws LoaderException 
	 * @throws InterruptedException 
	 * @throws CallPausedException 
	 * @throws CallException 
	 */
	public void execute(Map<String, Object> input, Map<String, Object> outputs) throws LoaderException, CallException, CallPausedException, InterruptedException {
		
		StringBuilder s = new StringBuilder();
		s.append(constructFields());
		s.append(constructPort(input));
		s.append(parent.behaviour);
		
		String program = s.toString();
		
		StateContext state = StateContexts.newDefaultInstance();
		Table env = StandardLibrary.in(RuntimeEnvironments.system()).installInto(state);
		
		ChunkLoader loader = CompilerChunkLoader.of("example");
		LuaFunction main = loader.loadTextChunk(new Variable(env), "example", program);
		
		DirectCallExecutor.newExecutor().call(state, main);
		
		Table out = (Table) env.rawget("output");
		
		Iterator<Entry<String, Object>> it = outputs.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, Object> entry = it.next();
			Object o = out.rawget(entry.getKey());
			if (o instanceof ByteString)
				o = o.toString();
			outputs.put(entry.getKey(), o);
		}
		
	}
	
	private String constructFields() {
		StringBuilder s = new StringBuilder("fields = {");
		Iterator<Entry<String, LuaVariable>> it = fields.entrySet().iterator();
		while(it.hasNext()) {
			Map.Entry<String, LuaVariable> entry = (Map.Entry<String, LuaVariable>) it.next();
			s.append(entry.getKey()).append(" = ").append(entry.getValue().getVar().toString());
			if (it.hasNext())
				s.append(", ");
		}
		s.append("};\n");
		/*
		s.append("local function checkFields(tab, name, value)\n" + 
				"if rawget(ReadOnly, name) then\n" + 
				"error(name ..' is a read only variable', 2)\n" + 
				"end\r\n" + 
				"rawset(tab, name, value)\n" + 
				"end\n");
		s.append("setmetatable(_G, {__index=fields, __newindex=checkFields})\n");
		*/
		return s.toString();
	}
	
	private String constructPort(Map<String, Object> input) {
		StringBuilder s = new StringBuilder("input = {");
		Iterator<Entry<String, Object>> it = input.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, Object> entry = it.next();
			s.append(entry.getKey()).append(" = ");
			if (portlist.get(entry.getKey()).getType() == Signal.Type.STRING) {
				s.append("'").append(entry.getValue().toString()).append("'");
			} else {
				s.append(entry.getValue().toString());
			}
			if (it.hasNext())
				s.append(", ");
		}
		s.append("};\n");
		/*
		s.append("local function checkInputs(tab, name, value)\n" + 
				"if rawget(ReadOnly, name) then\n" + 
				"error(name ..' is a read only variable', 2)\n" + 
				"end\r\n" + 
				"rawset(tab, name, value)\n" + 
				"end\n");
		s.append("setmetatable(_G, {__index=in, __newindex=checkInputs})\n");
		*/
		s.append("output = {};\n");
		
		return s.toString();
	}

}
