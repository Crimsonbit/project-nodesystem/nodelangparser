/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.fail;

import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

import htl.gtm.nodeLangParser.NodeLangParser;
import htl.gtm.nodeLangParser.Instance;
import htl.gtm.nodeLangParser.signal.Input;
import htl.gtm.nodeLangParser.signal.LuaVariable;
import htl.gtm.nodeLangParser.signal.Output;
import htl.gtm.nodeLangParser.signal.Signal;
import htl.gtm.nodeLangParser.signal.Wire;
import net.sandius.rembulan.exec.CallException;
import net.sandius.rembulan.exec.CallPausedException;
import net.sandius.rembulan.load.LoaderException;

public class UnitTest1 {
	
	@Test
	void test() throws URISyntaxException, LoaderException, CallException, CallPausedException, InterruptedException  {
		NodeLangParser parser = new NodeLangParser();
		parser.setTargetPath(Paths.get("src", "test", "resources", "unitTest1", "src.nl"));
		try {
			parser.parse();
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		
		Wire isEqual = (Wire) parser.getSignalMap().get("isEqual");
		
		Instance comp = parser.getInstances().get("comp");
		Instance adder = parser.getInstances().get("adder");
		
		Instance adderAInput = parser.getInstances().get("adderAInput");
		Instance adderBInput = parser.getInstances().get("adderBInput");
		
		Instance add3toMe = parser.getInstances().get("add3toMe");
		
		Instance add3Input = parser.getInstances().get("add3Input");
		
		Instance subtractor = parser.getInstances().get("subtractor");
		
		Wire subOut = (Wire) parser.getSignalMap().get("subOut");
		
		Instance comperatorA = parser.getInstances().get("comperatorA");
		
		//Checking Netlist
		
		//adderAInput.out = adder.a
		assertTrue(((Signal) ((Output) adderAInput.getPortlist().get("out")).getConsumers().get(0)).getName().equals("adder.a"));
		
		//adderBInput.out = adder.b
		assertTrue(((Signal) ((Output) adderBInput.getPortlist().get("out")).getConsumers().get(0)).getName().equals("adder.b"));
		
		//add3Input.out = add3toMe.in
		assertTrue(((Signal) ((Output) add3Input.getPortlist().get("out")).getConsumers().get(0)).getName().equals("add3toMe.input"));
	
		//subtractor.a = adder.c
		assertTrue(((Signal) ((Input) subtractor.getPortlist().get("a")).getProducer()).getName().equals("adder.c"));
		
		//subtractor.b = add3toMe.out
		assertTrue(((Signal) ((Input) subtractor.getPortlist().get("b")).getProducer()).getName().equals("add3toMe.out"));
		
		//subOut = subtractor.c
		assertTrue(((Signal)subOut.getProducer()).getName().equals("subtractor.c"));
		
		//comp.a = comperatorA.out
		assertTrue(((Signal) ((Input) parser.getSignalMap().get("comp.a")).getProducer()).getName().equals("comperatorA.out"));
		
		//comp.b = subtractor.c
		assertTrue(((Signal) ((Input) parser.getSignalMap().get("comp.b")).getProducer()).getName().equals("subtractor.c"));
		
		//isEqual = comp.equal
		assertTrue(((Signal) ((Wire) parser.getSignalMap().get("isEqual")).getProducer()).getName().equals("comp.equal"));
		
		//Compute node results
		Map<String, Object> adderAInputMap = new HashMap<>();
		adderAInputMap.put("out", null);
		adderAInput.execute(new HashMap<>(), adderAInputMap);
		
		double adderA = (double) adderAInputMap.get("out");
		assertTrue(adderA == 3D);
		
		
		Map<String, Object> adderBInputMap = new HashMap<>();
		adderBInputMap.put("out", null);
		adderBInput.execute(new HashMap<>(), adderBInputMap);
		
		double adderB = (double) adderBInputMap.get("out");
		assertTrue(adderB == 2D);
		
		
		Map<String, Object> add3InputMap = new HashMap<>();
		add3InputMap.put("out", null);
		add3Input.execute(new HashMap<>(), add3InputMap);
		
		double add3toMeIn = (double) add3InputMap.get("out");
		assertTrue(add3toMeIn == 2D);
		
		
		Map<String, Object> add3toMeInputMap = new HashMap<>();
		add3toMeInputMap.put("input", add3toMeIn);
		Map<String, Object> add3toMeOutputMap = new HashMap<>();
		add3toMeOutputMap.put("out", null);
		add3toMe.execute(add3toMeInputMap, add3toMeOutputMap);
		
		double add3toMeOut = (double) add3toMeOutputMap.get("out");
		assertTrue(add3toMeOut == 5D);
		
		//Abkuerzung
		Map<String, Object> compInputMap = new HashMap<>();
		compInputMap.put("a", 5D);
		compInputMap.put("b", add3toMeOut);
		Map<String, Object> compOutputMap = new HashMap<>();
		compOutputMap.put("equal", null);
		comp.execute(compInputMap, compOutputMap);
		
		boolean compOut = (boolean) compOutputMap.get("equal");
		assertTrue(compOut);
	}
	

}
