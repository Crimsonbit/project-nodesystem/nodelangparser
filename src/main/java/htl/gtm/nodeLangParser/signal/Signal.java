/*
Copyright 2018 Alexander Daum, Florian Wagner and Clemens Lechner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package htl.gtm.nodeLangParser.signal;

import java.util.Arrays;

import htl.gtm.nodeLangParser.exception.SignalDriveException;
import htl.gtm.nodeLangParser.exception.TypeMismatchException;
import net.sandius.rembulan.Table;
import net.sandius.rembulan.runtime.LuaFunction;

/**
 * The main type for routing signal paths<br>
 * 
 * Has a {@link Type} and a name
 * 
 * @see Consumer
 * @see Producer
 * 
 * @author Clemens
 *
 */
public abstract class Signal {

	/**
	 * Enum of possible Lua types
	 * 
	 * @author Clemens
	 *
	 */
	public enum Type {
		NIL("nil", void.class), BOOLEAN("boolean", Boolean.class), NUMBER("number", Number.class),
		STRING("string", String.class), FUNCTION("function", LuaFunction.class), USERDATA("userdata", Object.class),
		THREAD("thread", Thread.class), TABLE("table", Table.class);

		private final String name;
		private final Class<?> type;

		Type(String name, Class<?> type) {
			this.name = name;
			this.type = type;
		}

		/**
		 * 
		 * @return the Lua representational name of the {@link Type}
		 */
		public String getName() {
			return name;
		}

		@Override
		public String toString() {
			return this.getName();
		}

		/**
		 * Converts from a {@link String} Lua representation to a {@link Type}
		 * 
		 * @param s the {@link String} to convert from
		 * @return the {@link} type that it convertes to. In case of error returns
		 *         {@code null}
		 */
		public static Type parseType(String s) {
			return Arrays.stream(Type.values()).filter(type -> type.getName().equals(s)).findAny().orElse(null);
		}

		public Class<?> getType() {
			return type;
		}
	}

	private Type type;
	private String name;

	public Signal(String name) {
		this.name = name;
		this.type = Type.USERDATA;
	}

	public Signal(String name, Type type) {
		this.name = name;
		this.type = type;
	}

	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Tries to connect {@code this} {@link Signal} to another {@link Signal}<br>
	 * In case of {@link Wire}s (both {@link Consumer} and {@link Producer}) tries
	 * to connect other signal as Producer
	 * 
	 * @param other   Signal to connect to. In case of both {@link Wire}s connects
	 *                other as Producer of {@code this}
	 * @param section the section that tries to connect both signals (used for error
	 *                handling)
	 * @throws TypeMismatchException
	 * @throws SignalDriveException
	 */
	public void connect(Signal other, String section) throws TypeMismatchException, SignalDriveException {
		if (this instanceof Consumer && other instanceof Producer) {
			if (((Consumer) this).getProducer() == null) {
				//connect if types are equal or consumer is userdata
				if (this.getType().equals(other.getType()) || this.getType().equals(Type.USERDATA)) {
					((Consumer) this).setProducer((Producer) other);
					((Producer) other).getConsumers().add((Consumer) this);
				} else {
					throw new TypeMismatchException(other.getType(), this.getType(), section);
				}
			} else {
				throw new SignalDriveException(
						"Signal \"" + this.getName() + "\" can't be driven by more than one signal!", section);
			}
		} else if (this instanceof Producer && other instanceof Consumer) {
			if (((Consumer) other).getProducer() == null) {
				//connect if types are equal or consumer is userdata
				if (this.getType().equals(other.getType()) || other.getType().equals(Type.USERDATA)) {
					((Consumer) other).setProducer((Producer) this);
					((Producer) this).getConsumers().add((Consumer) other);
				} else {
					throw new TypeMismatchException(this.getType(), other.getType(), section);
				}
			} else {
				throw new SignalDriveException(
						"Signal \"" + other.getName() + "\" can't be driven by more than one signal!", section);
			}
		} else {
			throw new SignalDriveException(
					"Signal \"" + this.getName() + "\" can't be connected to signal \"" + other.getName() + "\"!",
					section);
		}
	}

}
